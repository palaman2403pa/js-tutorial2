console.log('tut3');
// Variables in js
// Var, let, const
var name = 'harry';
var channel;
var marks = 3454;
// channel = 'codewithHarry'
console.log(name, channel, marks)
// Rules for creating Javascript Variables
//*
// 1.Cannot start with numbers
// 2. can star with letter, numbers, or $
// 3. Are Case sensitive

var city = 'Delhi';
console.log(city);

const ownersName = 'Hari Ram';
// ownersname = 'harry';
console.log(ownersName);
const fruit = 'Orange';

{
    let city = 'Rampur';
    city = 'kolkata';
    console.log(city)
}
console.log(city);


const arr1 = [12,23,12,53, 3];
// arr1.push(45);
console.log(arr1)

